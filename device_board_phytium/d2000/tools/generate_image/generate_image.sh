#!/bin/sh
#############################################################################################################################
#manual:
#    this script is used to generate image of phytium android and openharmony product.
#tools needed: 
#    before use this script, make sure 3 tools intalled firstly. 
#    tools: 1. dosfstools  2. parted   3. kpartx
#    if not, install them with command sudo apt-get install
#usage:
#    how to generate image,  only 3 steps.
#        step1. copy system.img,vendor.img,ramdisk.img,kernel Image,product dtb file,part.cfg and this script to a directroy
#        step2. if want to change image name and partition size, modify them in part.cfg file. unit size in config file is
#               Mbytes.for example,boot_partition_size=256,It means allocate 256MBytes for boot partition. but for reason MBR
#		also occupy some disk space,actual size is a little smaller than config size. with command "sudo fdisk -l" ,
#		check the real size of all partitions after burned the image to disk.
#        step3. execute this script, if no err, an image will be generated in the directory.
#    how to burn image, It supports two ways.
#        in linux: sudo dd if=imagename of=/dev/sdX bs=1M
#        in windows:burn it with tool Win32DiskImager.exe, this tool can be downloaded from web very easily.
#company:
#	phytium
#verion:
#	V0.1.0 original
#	V0.1.1 fix mistake that node /dev/loopx was occupied by others.
############################################################################################################################


set -o errexit
##############################################################################################################################
#set color defination in echo info
green="\e[32;1m"
red="\e[31;1m"
normal="\e[0m"
##############################################################################################################################

##############################################################################################################################

#partion sizes are defined in part.cfg, Unit size is MByte
CONFIG=part.cfg

IMAGE_NAME=$(awk '/^image_name/{print $3}' "$CONFIG")
LABLE_RESERVE_SIZE=$(awk '/^label_reserve_size/{print $3}' "$CONFIG")
BOOT_PARTION_SIZE=$(awk '/^boot_partition_size/{print $3}' "$CONFIG")
SYSTEM_PARTION_SIZE=$(awk '/^system_partition_size/{print $3}' "$CONFIG")
VENDOR_PARTION_SIZE=$(awk '/^vendor_partition_size/{print $3}' "$CONFIG")
USERDATA_PARTION_SIZE=$(awk '/^userdata_partition_size/{print $3}' "$CONFIG")

START_BOOT_ADDR=$LABLE_RESERVE_SIZE
let END_BOOT_ADDR=$LABLE_RESERVE_SIZE+$BOOT_PARTION_SIZE
START_SYSTEM_ADDR=$END_BOOT_ADDR
let END_SYSTEM_ADDR=$START_SYSTEM_ADDR+$SYSTEM_PARTION_SIZE
START_VENDOR_ADDR=$END_SYSTEM_ADDR
let END_VENDOR_ADDR=$START_VENDOR_ADDR+$VENDOR_PARTION_SIZE
START_USERDATA_ADDR=$END_VENDOR_ADDR
let END_USERDATA_ADDR=$START_USERDATA_ADDR+$USERDATA_PARTION_SIZE
let TOTAL_IMAGE_SIZE=$LABLE_RESERVE_SIZE+$BOOT_PARTION_SIZE+$SYSTEM_PARTION_SIZE+$VENDOR_PARTION_SIZE+$USERDATA_PARTION_SIZE
###############################################################################################################################

sudo -v
echo -e "${green}start creating empty image, please wait......${normal}"
sudo dd if=/dev/zero of=$IMAGE_NAME bs=1MB count=$TOTAL_IMAGE_SIZE
echo -e "${green}parting image ......${normal}"
sudo parted $IMAGE_NAME --script -- mklabel msdos
sudo parted $IMAGE_NAME --script -- mkpart primary ext4 ${START_BOOT_ADDR}M ${END_BOOT_ADDR}M-1
sudo parted $IMAGE_NAME --script -- mkpart primary ext4 ${START_SYSTEM_ADDR}M ${END_SYSTEM_ADDR}M-1
sudo parted $IMAGE_NAME --script -- mkpart primary ext4 ${START_VENDOR_ADDR}M ${END_VENDOR_ADDR}M-1
sudo parted $IMAGE_NAME --script -- mkpart primary ext4 ${START_USERDATA_ADDR}M ${END_USERDATA_ADDR}M-1

loopdevice=`sudo losetup -f --show ${IMAGE_NAME}`
device=`sudo kpartx -va $loopdevice | sed -E 's/.*(loop[1-9]?[0-9])p.*/\1/g' | head -1`
device="/dev/mapper/${device}"
partBoot="${device}p1"
partSystem="${device}p2"
partVendor="${device}p3"
partUserdata="${device}p4"
echo -e "${green}start to generate boot partition......${normal}"
sudo mkfs.ext4 -L boot $partBoot
sudo mkdir -p  /media/image_to_boot
sudo mount $partBoot /media/image_to_boot
sudo cp Image ramdisk.img *.dtb part.cfg /media/image_to_boot
sudo umount /media/image_to_boot
sudo rm /media/image_to_boot -rf

echo -e "${green}start dd system and vendor image......${normal}"
sudo dd if=system.img of=$partSystem bs=1M
sudo dd if=vendor.img of=$partVendor bs=1M

echo -e "${green}start to generate userdata partition......${normal}"
sudo mkfs.ext4 -L userdata $partUserdata

sync
sleep 5

echo "kpart loopdevice......"
sudo kpartx -d $loopdevice
echo "losetup loopdevice......"
sudo losetup -d $loopdevice
echo -e "${green}generate $IMAGE_NAME successfully!!!!!! ${normal}"
